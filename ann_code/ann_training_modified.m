[tvec tlab tstv tstl] = readSets(); 
[unique(tlab)'; unique(tstl)']
tlab += 1;
tstl += 1;

[unique(tlab)'; sum(tlab == unique(tlab)')]
[unique(tstl)'; sum(tstl == unique(tstl)')]

noHiddenNeurons = 100;
noEpochs = 200;
lr = 0.001
%rand()

%load hlnnBI.txt
%load olnnBI.txt
%hlnnMod = hlnn;
%olnnMod = olnn;

% loading state of (pseudo)random number generator
load rep_h100_e200_lr001rndstate.txt 
rand("state", rndstate);

[hlnn olnn] = crann(columns(tvec), noHiddenNeurons, 10);
trainError = zeros(1, noEpochs);
testError = zeros(1, noEpochs);
trReport = [];

%%%% DATA NORMALIZATION %%%%%
tvecNorm = tvec - repmat(mean(tvec),rows(tvec),1);
tstvNorm = tstv - repmat(mean(tvec),rows(tstv),1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialization of error for results with modifications
trainErrorMod = zeros(1, noEpochs);
testErrorMod = zeros(1, noEpochs);


for epoch=1:noEpochs
%%%% REFERENCED METHOD %%%%%
%	tic();
%	[hlnn olnn terr] = backprop(tvec, tlab, hlnn, olnn, lr);
%	clsRes = anncls(tvec, hlnn, olnn);
%	cfmx = confMx(tlab, clsRes);
%	errcf = compErrors(cfmx);
%	trainError(epoch) = errcf(2);

%	clsRes = anncls(tstv, hlnn, olnn);
%	cfmx = confMx(tstl, clsRes);
%	errcf2 = compErrors(cfmx);
%	testError(epoch) = errcf2(2);	
	
%	epochTime = toc();
		
%	disp([epoch epochTime trainError(epoch) testError(epoch)])
	%trReport = [trReport; epoch epochTime trainError(epoch) testError(epoch)];
%	[errcf errcf2]
%	fflush(stdout);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% DATA NORMALIZATION %%%%%	
%	tic();
%	[hlnn olnn terr] = backprop(tvecNorm, tlab, hlnn, olnn, lr);
%	clsRes = anncls(tvecNorm, hlnn, olnn);
%	cfmx = confMx(tlab, clsRes);
%	errcf = compErrors(cfmx);
%	trainError(epoch) = errcf(2);

%	clsRes = anncls(tstvNorm, hlnn, olnn);
%	cfmx = confMx(tstl, clsRes);
%	errcf2 = compErrors(cfmx);
%	testError(epoch) = errcf2(2);
%	epochTime = toc();
%	disp([epoch epochTime trainError(epoch) testError(epoch)])
%	fflush(stdout);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% ADAPTIVE LEARNING RATE %%%%%
%	tic();
%	lr = 0.01 - ((epoch-1)*0.0001) % example for 200 epochs: 0.01- ((epoch-1)*0.00005)
	%lr = 0.01/epoch
%	[hlnn olnn terr] = backprop(tvec, tlab, hlnn, olnn, lr);
%	clsRes = anncls(tvec, hlnn, olnn);
%	cfmx = confMx(tlab, clsRes);
%	errcf = compErrors(cfmx);
%	trainError(epoch) = errcf(2);

%	clsRes = anncls(tstv, hlnn, olnn);
%	cfmx = confMx(tstl, clsRes);
%	errcf2 = compErrors(cfmx);
%	testError(epoch) = errcf2(2);
%	epochTime = toc();
%	disp([epoch epochTime trainError(epoch) testError(epoch)])
%	fflush(stdout);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
%%%% INERTION %%%%%	
%	tic();
%	[hlnn olnn terr] = backprop_inertion(tvec, tlab, hlnn, olnn, lr, 0.5);
%	clsRes = anncls(tvec, hlnn, olnn);
%	cfmx = confMx(tlab, clsRes);
%	errcf = compErrors(cfmx);
%	trainError(epoch) = errcf(2);

%	clsRes = anncls(tstv, hlnn, olnn);
%	cfmx = confMx(tstl, clsRes);
%	errcf2 = compErrors(cfmx);
%	testError(epoch) = errcf2(2);
%	epochTime = toc();
%	disp([epoch epochTime trainError(epoch) testError(epoch)])
%	fflush(stdout);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
%%%% ALL %%%%%	
	tic();
	lr = 0.01/epoch
	[hlnn olnn terr] = backprop_inertion(tvecNorm, tlab, hlnn, olnn, lr, 0.5);
	clsRes = anncls(tvecNorm, hlnn, olnn);
	cfmx = confMx(tlab, clsRes);
	errcf = compErrors(cfmx);
	trainError(epoch) = errcf(2);

	clsRes = anncls(tstvNorm, hlnn, olnn);
	cfmx = confMx(tstl, clsRes);
	errcf2 = compErrors(cfmx);
	testError(epoch) = errcf2(2);
	epochTime = toc();
	disp([epoch epochTime trainError(epoch) testError(epoch)])
	fflush(stdout);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

save rep_h100_e20_lr001_reference.txt trReport 

plot(1:200, trainError, 'b', 1:200, testError, 'r')
xlabel('epoch');
ylabel('error [%]');
title ("training and testing error during backprop");
legend ("train error", "test error"); 
grid on
%set(findall(gcf,'-property','FontSize'),'FontSize',12);
