function res = actf_bi(tact)
% sigmoid activation function (transpose to range (-1,1) and consist point (0,0))
% tact - total activation 
	
	res = 2./(1+exp(-tact))-1;
